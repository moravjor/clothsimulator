To play a recorded simulation run:
```bash
clothsimulator -p <filename>
# or
clothsimulator -p <filename>
```

For example:
```bash
# play simulation saved in file long_simulation.record 4x faster
clothsimulator --play long_simulation.record --playback-speed 4
```

Here is a list of recorded simulations with commands that were used to generate these animations.

# Recorded Simulations

## structural_springs_low_stiffness.record

The structural springs hold the vertices together, however the stiffness is too low
so the springs are too loose.

```bash
clothsimulator \
    --scene flag_pinned_down \
    --flexion-stiffness 0 \
    --shear-stiffness 0 \
    --shear-damping 0 \
    --structural-damping 0 \
    --structural-stiffness 0.02 \
    --simulation-fps 25 \
    -r recorded_simulations \
    -f structural_springs_low_stiffness.record \
    -t 20
```

## unstable.record

When we increase the stiffness so the forces are higher we run into a stability problem 
caused by the big simulation time step.

```bash
clothsimulator \
    --scene flag_pinned_down \
    --flexion-stiffness 0 \
    --shear-stiffness 0 \
    --shear-damping 0 \
    --simulation-fps 50 \
    -r recorded_simulations \
    -f unstable.record \
    -t 20
```

## structural_springs_undampped.record

When we increase the simulation time step, we can afford higher stiffness, which now
looks much more realistic. However, there is no damping to the springs so they will
bounce like crazy forever.

```bash
clothsimulator \
    --scene flag_pinned_down \
    --flexion-stiffness 0 \
    --shear-stiffness 0 \
    --shear-damping 0 \
    --structural-damping 0 \
    -r recorded_simulations \
    -f structural_springs_undampped.record \
    -t 20
```

## structural_springs.record

Now the cloth will calm down after a while. But look at the fixed corners of the cloth.
There is no shear spring yet to help hold the cloth structure.

```bash
clothsimulator \
    --scene flag_pinned_down \
    --flexion-stiffness 0 \
    --shear-stiffness 0 \
    --shear-damping 0 \
    -r recorded_simulations \
    -f structural_springs.record \
    -t 20
```

## shear_springs.record

The cloth acts much more naturally now and holds it's shape.
However when you rotate the scene to see the cloth from the side,
you see how it folds unnaturally.

```bash
clothsimulator \
    --scene flag_pinned_down \
    --flexion-stiffness 0 \
    -r recorded_simulations \
    -f shear_springs.record \
    -t 20
```

## flexion_springs_undampped.record

The cloth want to unfold, but without damping it moves like in strong wind.

```bash
clothsimulator -r recorded_simulations -t 20 -f flexion_springs_undampped.record
```

