from setuptools import setup, find_packages
from clothsimulator.version import version


setup(
    name="clothsimulator",
    version=version,
    description="Cloth Simulator for MMA class",
    packages=find_packages(),
    author="Jordan Moravenov",
    author_email="j.moravenov@gmail.com",
    entry_points={
        "console_scripts": [
            "clothsimulator = clothsimulator.main:main",
            "clothsimulator_demo = clothsimulator.main:demo_main"
        ]
    },
    install_requires=[
        "setuptools",
        "pygame>=1.9.4",
        "PyOpenGL>=3.1.0",
        "numpy>=1.15.4",
        "scipy>=1.2.0",
        "nose>=1.3.7",
    ],
    package_data={
        "clothsimulator": []
    }
)
