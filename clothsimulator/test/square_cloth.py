from clothsimulator.simulation import Cloth, ShearSpring, FlexionSpring


class Square(Cloth):
    """
    6 -- 7 -- 8
    |    |    |
    3 -- 4 -- 5
    |    |    |
    0 -- 1 -- 2
    """
    def __init__(self):
        vertices = [
            (-1.0, -1.0, 0.0), (0.0, -1.0, 0.0), (1.0, -1.0, 0.0),
            (-1.0, 0.0, 0.0), (0.0, 0.0, 0.0), (1.0, 0.0, 0.0),
            (-1.0, 1.0, 0.0), (0.0, 1.0, 0.0), (1.0, 1.0, 0.0),
        ]
        edges = [
            (0, 1), (1, 2), (3, 4), (4, 5), (6, 7), (7, 8),
            (0, 3), (3, 6), (1, 4), (4, 7), (2, 5), (5, 8),
        ]
        self.expected_shear_springs = [
            [0, 4], [1, 5], [3, 7], [4, 8],
            [1, 3], [2, 4], [4, 6], [5, 7]
        ]
        self.expected_flexion_springs = [
            [1, 7], [3, 5]
        ]
        super(Square, self).__init__("square", vertices, edges)

    def init_shear_springs_as_expected(self):
        for vertex1_index, vertex2_index in self.expected_shear_springs:
            spring = ShearSpring(self.vertices[vertex1_index], self.vertices[vertex2_index])
            self.vertices[vertex1_index].add_shear_spring(spring)
            self.vertices[vertex2_index].add_shear_spring(spring)

    def init_flexion_springs_as_expected(self):
        v1 = self.vertices[1]
        v3 = self.vertices[3]
        v4 = self.vertices[4]
        v5 = self.vertices[5]
        v7 = self.vertices[7]

        s1 = FlexionSpring(v1, v7, v4)
        v1.add_flexion_spring(s1)
        v7.add_flexion_spring(s1)

        s2 = FlexionSpring(v3, v5, v4)
        v3.add_flexion_spring(s2)
        v5.add_flexion_spring(s2)

