from typing import Set, List, FrozenSet

from nose.tools import (
    assert_equal, assert_set_equal, assert_greater, assert_less, assert_almost_equal,
    assert_tuple_equal)
import numpy as np

import clothsimulator.geometry as geo 
from clothsimulator.simulation import StretchSpring, FlexionSpring
from clothsimulator.test.square_cloth import Square


# class TestStructuralSpring(object):
#     def setup(self):
#         self.v1 = geo.Vertex(0, 0.0, 0.0, 0.0)
#         self.v2 = geo.Vertex(1, 1.0, 0.0, 0.0)
#         self.spring = StructuralSpring(self.v1, self.v2)
#
#     def test_force_should_be_zero_for_rest_length(self):
#         assert_equal(self.spring._get_spring_force(), 0.0)
#
#     def test_spring_should_want_to_shrink_on_strech(self):
#         self.v2.position += numpy.array([0.5, 0.0, 0.0])
#         assert_greater(self.spring._get_spring_force(), 0.0)
#
#     def test_spring_should_want_to_strech_on_shrink(self):
#         self.v2.position += numpy.array([-0.5, 0.0, 0.0])
#         assert_less(self.spring._get_spring_force(), 0.0)


class TestCloth(object):
    def test_init_shear_springs(self):
        def to_set_of_sets(l: List[List[int]]) -> Set[FrozenSet[int]]:
            return set(frozenset(x) for x in l)

        sq = Square()

        shear_springs: Set[StretchSpring] = set()
        for v in sq.vertices:
            for spring in v.shear_springs:
                shear_springs.add(spring)

        result_shear_springs: List[List[int]] = []
        for s in shear_springs:
            result_shear_springs.append([s.vertex1.id, s.vertex2.id])

        assert_set_equal(
            to_set_of_sets(result_shear_springs),
            to_set_of_sets(sq.expected_shear_springs)
        )

    def test_init_flexion_springs(self):
        def to_set_of_sets(l: List[List[int]]) -> Set[FrozenSet[int]]:
            return set(frozenset(x) for x in l)

        sq = Square()

        flexion_springs: Set[FlexionSpring] = set()
        for v in sq.vertices:
            for spring in v.flexion_springs:
                flexion_springs.add(spring)

        result_flexion_springs: List[List[int]] = []
        for s in flexion_springs:
            result_flexion_springs.append([s.vertex1.id, s.vertex2.id])

        assert_set_equal(
            to_set_of_sets(result_flexion_springs),
            to_set_of_sets(sq.expected_flexion_springs)
        )


class TestVertex(object):
    def test_substitution(self):
        v1 = geo.Vertex(0, 2.0, 0.0, 0.0)
        v2 = geo.Vertex(1, 0.5, 0.0, 0.0)

        vector = v1 - v2
        assert_equal(vector[0], 1.5)
        vector = v2 - v1
        assert_equal(vector[0], -1.5)

        assert_equal(type(vector), np.ndarray)

    def test_rounding(self):
        vertex = geo.Vertex(0, 2.000000001, 1.99999, 1.23456)
        assert_tuple_equal(
            vertex.get_tuple(2),
            (2.0, 2.0, 1.23)
        )


class TestFlexionSpringAngle(object):
    def test180(self):
        v1 = geo.Vertex(0, 0.0, 0.0, 0.0)
        vm = geo.Vertex(2, 0.2, 0.0, 0.0)
        v2 = geo.Vertex(3, 1.0, 0.0, 0.0)

        assert_almost_equal(geo.angle(v1 - vm, v2 - vm), np.deg2rad(180))

    def test90(self):
        v1 = geo.Vertex(0, 0.0, 0.0, 0.0)
        vm = geo.Vertex(2, 1.0, 0.0, 0.0)
        v2 = geo.Vertex(3, 1.0, 1.0, 0.0)

        assert_almost_equal(geo.angle(v2 - vm, v1 - vm), np.deg2rad(90))
        assert_almost_equal(geo.angle(v1 - vm, v2 - vm), np.deg2rad(90))

    def test45(self):
        v1 = geo.Vertex(0, 1.0, 1.0, 0.0)
        vm = geo.Vertex(2, 0.0, 0.0, 0.0)
        v2 = geo.Vertex(3, 1.0, 0.0, 0.0)

        assert_almost_equal(geo.angle(v1 - vm, v2 - vm), np.deg2rad(45))
