"""Command line arguments."""
import argparse

import clothsimulator.simulation as sim

LIST_OF_SCENES = [
    "flag_pinned_up", "flag_pinned_down", "string_pinned_one", "string_pinned_two", "weight"
]


def get_parser():
    parser = argparse.ArgumentParser(
        description="Cloth simulation program (mass-spring model)"
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--record", "-r", type=str, default=None, metavar="DIRECTORY",
        help="Directory, where the recorded animation will be saved. "
             "The application enters recording mode when this argument is used."
    )
    group.add_argument(
        "--play", "-p", type=str, default=None, metavar="FILE",
        help="File containing recorded animation, that will be played. "
             "The application enters playing mode when this argument is used."
    )

    parser.add_argument(
        "--record-filename", "-f", type=str, default="simulation.record", metavar="FILE_NAME",
        help="Name of the saved animation file. (default: simulation.record)"
    )
    parser.add_argument(
        "--recording-time", "-t", type=int, default=None, metavar="SECONDS",
        help="Number of seconds of the simulation to be recorded."
             "If not specified, the simulation will wait for you to save it manually by pressing S."
    )
    parser.add_argument(
        "--scene", type=str, default="flag_pinned_up", metavar="MODE", choices=LIST_OF_SCENES,
        help="Choose from a predefined scenes. This option is ignored in playing mode. "
             "(default: flag_pinned_up) OPTIONS: %s"
             % (", ".join(map(lambda s: "'%s'" % (s,), LIST_OF_SCENES)),)
    )
    parser.add_argument(
        "--fps", type=int, default=25, metavar="FRAMES_PER_SECOND",
        help="How often the image is rendered to the screen. "
             "In recording mode it also specifies the FPS for the recorded animation. "
             "In playing mode it also affects the playback speed. "
             "Rather use the same FPS for recording and playing and change playback speed "
             "using --playback-speed "
             "(default: 25)"
    )
    parser.add_argument(
        "--simulation-fps", "-s", type=int, default=250, metavar="SIMULATION_STEPS_PER_SECOND",
        help="How many simulation steps will be calculated per second. (default: 250)"
    )
    parser.add_argument(
        "--playback-speed", "-v", type=float, default=1.0, metavar="PLAYBACK_SPEED",
        help="Set playback speed. 1.0 will play it in recorded speed. 0.5 will play the animation "
             "twice slower, 2.0 will play the animation twice faster."
    )
    parser.add_argument(
        "--window-width", "-w", type=int, default=800, metavar="PIXEL_COUNT",
        help="Window width in pixels. (The window is rectangular)"
    )
    parser.add_argument(
        "--hide-axis", action="store_true", help="Don't render 3D space axis."
    )
    parser.add_argument(
        "--structural-stiffness", type=float, default=sim.StructuralSpring.DEFAULT_STIFFNESS,
        help="default: %s" % (str(sim.StructuralSpring.DEFAULT_STIFFNESS),)
    )
    parser.add_argument(
        "--structural-damping", type=float, default=sim.StructuralSpring.DEFAULT_DAMPING_FACTOR,
        help="default: %s" % (str(sim.StructuralSpring.DEFAULT_DAMPING_FACTOR),)
    )
    parser.add_argument(
        "--shear-stiffness", type=float, default=sim.ShearSpring.DEFAULT_STIFFNESS,
        help="default: %s" % (str(sim.ShearSpring.DEFAULT_STIFFNESS),)
    )
    parser.add_argument(
        "--shear-damping", type=float, default=sim.ShearSpring.DEFAULT_DAMPING_FACTOR,
        help="default: %s" % (str(sim.ShearSpring.DEFAULT_DAMPING_FACTOR),)
    )
    parser.add_argument(
        "--flexion-stiffness", type=float, default=sim.FlexionSpring.DEFAULT_STIFFNESS,
        help="default: %s" % (str(sim.FlexionSpring.DEFAULT_STIFFNESS),)
    )
    parser.add_argument(
        "--vertex-mass", type=float, default=sim.Cloth.DEFAULT_VERTEX_MASS, metavar="KILOGRAMS",
        help="default: %skg" % (str(sim.Cloth.DEFAULT_VERTEX_MASS))
    )
    return parser
