import json
import os
from time import time
from typing import List

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

from clothsimulator.argument_parser import get_parser
import clothsimulator.geometry as geo
import clothsimulator.simulation as sim
from clothsimulator.test.square_cloth import Square


class Cube(geo.Mesh):
    """Cube in the center of 3D space for testing purposes."""
    def __init__(self):
        vertices = [
            (1, -1, -1), (1, 1, -1), (-1, 1, -1), (-1, -1, -1),
            (1, -1, 1), (1, 1, 1), (-1, -1, 1), (-1, 1, 1),
        ]
        edges = [
            (0, 1), (0, 3), (0, 4), (2, 1), (2, 3), (2, 7),
            (6, 3), (6, 4), (6, 7), (5, 1), (5, 4), (5, 7),
        ]
        super(Cube, self).__init__(vertices, edges)


class Flag(sim.Cloth, geo.Plane):
    """Plane mesh with cloth simulation."""
    def __init__(self, name: str, *args, **kwargs):
        geo.Plane.__init__(self, *args, **kwargs)
        self.name = name
        self.init_cloth(sim.Cloth.vertex_mass)


def demo_main():
    """Testing main to play with PyOpenGL."""
    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    # field of view, ratio, min clipping distance, max clipping distance
    gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)

    glTranslatef(0.0, 0.0, -5.0)

    cube = Cube()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        cube.draw_lines()
        pygame.display.flip()
        glRotatef(1, 1, 1, 1)
        pygame.time.wait(10)


class Timer(object):
    def __init__(self, current_time):
        self.previous = current_time
        self.delta = 0.0
        self.current = current_time

    def update_delta(self):
        self.current = time()
        self.delta = self.current - self.previous

    def new_cycle(self):
        self.previous = self.current


def mk_static_objects(hide_axis=False) -> List[geo.Mesh]:
    objects = []
    if not hide_axis:
        x_axis = geo.Plane(2, 1, vertex_step=20)
        y_axis = geo.Plane(1, 2, vertex_step=20)
        x_axis.set_color(0.8, 0.2, 0.2)
        y_axis.set_color(0.2, 0.8, 0.2)
        objects.append(x_axis)
        objects.append(y_axis)
    return objects


def mk_cloth_objects(scene: str) -> List[sim.Cloth]:
    objects = []
    if scene == "weight":
        line = Flag("line", 1, 2)
        line.vertices[1].fixed = True
        line.vertices[0].mass *= 500
        objects.append(line)

    if scene.startswith("flag"):
        flag = Flag("flag", 15, 9, shear_to_side=True, position=(0.0, 10.0, 0.0))
        if scene == "flag_pinned_down":
            flag.vertices[0].fixed = True
            flag.vertices[flag.horizontal_vertex_count - 1].fixed = True
        elif scene == "flag_pinned_up":
            flag_vertex_count = flag.horizontal_vertex_count * flag.vertical_vertex_count
            flag.vertices[flag_vertex_count - 1].fixed = True
            flag.vertices[flag_vertex_count - flag.vertical_vertex_count].fixed = True
        objects.append(flag)

    if scene.startswith("string"):
        string = Flag("string", 15, 1)
        string.vertices[0].fixed = True
        if scene == "string_pinned_two":
            string.vertices[string.horizontal_vertex_count - 1].fixed = True
        objects.append(string)

    # square = Square()
    # square.vertices[0].fixed = True
    # square.vertices[2].fixed = True

    return objects


def save_records_and_quit(recording_dir, record_filename, records):
    filepath = os.path.join(recording_dir, record_filename)
    with open(filepath, "w") as f:
        f.write(json.dumps(records))
    quit_app("Simulation saved to '%s'. Quiting." % (filepath,))


def quit_app(message=None):
    if message is not None:
        print(message)
    pygame.quit()
    quit()


def main():
    # parse command line arguments and init application
    parser = get_parser()
    args = parser.parse_args()
    rendering_fps = args.fps
    simulation_fps = args.simulation_fps
    playing_file = args.play
    recording: bool = args.record is not None
    playing: bool = playing_file is not None
    if recording:
        recording_dir = os.path.abspath(args.record)
        if not os.path.isdir(recording_dir):
            quit_app("'%s' is not existing directory path" % (recording_dir,))
        record_filename = args.record_filename
        if os.path.isfile(os.path.join(recording_dir, record_filename)):
            quit_app(
                "File '%s' already exists in folder '%s'. "
                "Use --record-filename to choose different name"
                % (record_filename, recording_dir)
            )
    recording_time = args.recording_time
    autosave: bool = recording_time is not None
    window_width, window_height = args.window_width, args.window_width
    scene = args.scene
    hide_axis = args.hide_axis

    sim.Cloth.vertex_mass = args.vertex_mass
    sim.StructuralSpring.stiffness = args.structural_stiffness
    sim.StructuralSpring.damping_factor = args.structural_damping
    sim.ShearSpring.stiffness = args.shear_stiffness
    sim.ShearSpring.damping_factor = args.shear_damping
    sim.FlexionSpring.stiffness = args.flexion_stiffness

    rendering_step = 1 / rendering_fps
    simulation_step = 1 / simulation_fps
    simulation_steps_elapsed = 0
    playback_frames_elapsed = 0
    frames_to_be_recorded = 0 if recording_time is None else recording_time * rendering_fps
    saved_frames_counter = 0

    pygame.init()
    pygame.display.set_caption("Cloth Simulator")
    display = (window_width, window_height)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    gluPerspective(45, (display[0] / display[1]), 0.1, 100.0)
    glTranslatef(0.0, 0.0, -40.0)
    glRotatef(45, 0, 1, 0)

    static_objects: List[geo.Mesh] = mk_static_objects(hide_axis)
    current_time = time()
    rendering_timer = Timer(current_time)
    simulation_timer = Timer(current_time)

    records = {}
    playing_frame_count = 0
    if playing:
        with open(os.path.abspath(playing_file), "r") as f:
            records = json.loads(f.read())
        playing_frame_count = min(len(record) for record in records["frames"].values())
        cloth_objects: List[sim.Cloth] = mk_cloth_objects(records["scene"])
    else:
        cloth_objects: List[sim.Cloth] = mk_cloth_objects(scene)
        records["frames"] = {cloth.name: [] for cloth in cloth_objects}
        records["scene"] = scene

    mouse_down = False
    last_mouse_x, last_mouse_y = mouse_x, mouse_y = 0.0, 0.0
    holding_keys = [pygame.K_LCTRL]
    keys_down = {key: False for key in holding_keys}

    # main loop
    while True:
        # event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_app()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 2 or event.button == 1 and keys_down[pygame.K_LCTRL]:
                    mouse_down = True
                    last_mouse_x, last_mouse_y = mouse_x, mouse_y = event.pos
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button in [1, 2]:
                    mouse_down = False
            elif event.type == pygame.MOUSEMOTION:
                if mouse_down:
                    mouse_x, mouse_y = event.pos
                    x = (mouse_x - last_mouse_x) / window_width
                    y = (mouse_y - last_mouse_y) / window_height
                    last_mouse_x, last_mouse_y = mouse_x, mouse_y
                    rotate_scene(x, y)

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    cloth_objects = mk_cloth_objects(scene)
                    if playing:
                        playback_frames_elapsed = 0
                elif event.key == pygame.K_ESCAPE:
                    quit_app("Escape pressed. Quitting.")
                elif event.key in holding_keys:
                    keys_down[event.key] = True
                elif event.key == pygame.K_s:
                    if recording:
                        save_records_and_quit(recording_dir, record_filename, records)
                    else:
                        print("S key pressed: cannot SAVE, program not in recording mode.")

            elif event.type == pygame.KEYUP:
                if event.key in holding_keys:
                    keys_down[event.key] = False

        # running simulation
        if not playing:
            simulation_timer.update_delta()
            if simulation_timer.delta > simulation_step:
                # check_fps(simulation_timer.delta, SIMULATION_FPS)
                for o in cloth_objects:
                    o.simulate(simulation_step)  # <- not realtime if computer not quick enough
                    # o.simulate(simulation_timer.delta)  # <- realtime, but big error on slow computers
                simulation_timer.new_cycle()
                if recording:
                    if simulation_steps_elapsed % (simulation_fps // rendering_fps) == 0:
                        saved_frames_counter += 1
                        for o in cloth_objects:
                            records["frames"][o.name].append(o.get_vertex_tuples(2))
                    simulation_steps_elapsed += 1
                    if autosave and saved_frames_counter >= frames_to_be_recorded:
                        save_records_and_quit(recording_dir, record_filename, records)

        # rendering animation
        rendering_timer.update_delta()
        if rendering_timer.delta > rendering_step:
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            for o in static_objects + cloth_objects:
                o.draw_lines()
            rendering_timer.new_cycle()
            pygame.display.flip()
            if playing:
                frame_index = (
                    int(playback_frames_elapsed * args.playback_speed) % playing_frame_count
                )
                for o in cloth_objects:
                    o.update(records["frames"][o.name][frame_index])
                playback_frames_elapsed += 1


def check_fps(delta_t, expected_fps):
    real_fps = round(1 / delta_t, 2)
    if real_fps != expected_fps:
        print("FPS %f, expected: %f" % (real_fps, expected_fps))


def rotate_scene(x: float, y: float):
    """
    :param x: <-1; 1>
    :param y: <-1; 1>
    :return:
    """
    glRotatef(360 * x, 0, 1, 0)


if __name__ == '__main__':
    # demo_main()
    main()
