from OpenGL.GL import *
import numpy as np
from typing import List, Tuple


def normalize(vector: np.ndarray) -> np.ndarray:
    vector_length = length(vector)
    if vector_length == 0:
        return vector
    return vector / vector_length


def length(vector: np.ndarray) -> float:
    return np.linalg.norm(vector)


def angle(vector1: np.ndarray, vector2: np.ndarray):
    return np.arccos(
        np.clip(
            np.dot(normalize(vector1), normalize(vector2)),
            -1.0, 1.0
        )
    )


class Vertex(object):
    """
    :param vertex_id: should be unique in one mesh
    """
    def __init__(self, vertex_id: int, x: float, y: float, z: float):
        self.id = vertex_id
        self.position: np.ndarray = np.array([x, y, z])

    def __str__(self):
        return "Vertex(%d)" % self.id

    def __sub__(self, other):
        # type: (Vertex) -> np.ndarray
        return self.position - other.position

    def distance_from(self, vertex):
        # type: (Vertex) -> float
        return np.linalg.norm(self.position - vertex.position)

    def get_tuple(self, rounding=None):
        position = self.position
        if rounding is not None:
            r = 10 ** rounding
            position = map(lambda x: round(x * r) / r, position)
        return tuple(position)


class Mesh(object):
    """Mesh contains 3D geometry which can draw using OpenGL."""
    VERTEX_CLS = Vertex
    vertices: List[Vertex] = []
    color: Tuple[float, float, float] = (1.0, 1.0, 1.0)

    def __init__(
            self,
            vertices: List[Tuple[float, float, float]],
            edges: List[Tuple[int, int]],
    ):
        def mk_vertex(enumerated_vertices):
            i, (x, y, z) = enumerated_vertices
            return self.VERTEX_CLS(i, x, y, z)
        self.vertices = list(map(mk_vertex, enumerate(vertices)))
        self.edges: List[Tuple[int, int]] = edges

    def set_color(self, r: float, g: float, b: float):
        self.color = (r, g, b)

    def get_vertex_tuples(self, rounding=None):
        return list(map(lambda v: v.get_tuple(rounding=rounding), self.vertices))

    def draw_points(self):
        glBegin(GL_POINTS)
        for v in self.get_vertex_tuples():
            glVertex3fv(v)
        glEnd()

    def draw_lines(self):
        vertex_tuples = self.get_vertex_tuples()
        glBegin(GL_LINES)
        glColor3f(*self.color)
        for edge in self.edges:
            for vertex_id in edge:
                glVertex3fv(vertex_tuples[vertex_id])
        glEnd()


class Plane(Mesh):
    def __init__(
            self,
            horizontal_vertex_count: int,
            vertical_vertex_count: int,
            shear_to_side: bool = False,
            vertex_step: float = 1.0,
            position: Tuple[float, float, float] = (0.0, 0.0, 0.0)
    ):
        self.horizontal_vertex_count = horizontal_vertex_count
        self.vertical_vertex_count = vertical_vertex_count

        horizontal_step = vertex_step
        vertical_step = vertex_step

        x_pos, y_pos, z_pos = position

        width = (self.horizontal_vertex_count - 1) * horizontal_step
        height = (self.vertical_vertex_count - 1) * vertical_step

        vertices = [
            (
                x_pos + x * horizontal_step - width / 2,
                y_pos + y * vertical_step - height / 2,
                z_pos + y * (0.1 if shear_to_side else 0.0)
            )
            for y in range(self.vertical_vertex_count)
            for x in range(self.horizontal_vertex_count)
        ]

        horizontal_edges = [
            (x + y * self.horizontal_vertex_count, x + 1 + y * self.horizontal_vertex_count)
            for y in range(self.vertical_vertex_count)
            for x in range(self.horizontal_vertex_count - 1)
        ]

        vertical_edges = [
            (x + y * self.horizontal_vertex_count,
             x + self.horizontal_vertex_count + y * self.horizontal_vertex_count)
            for y in range(self.vertical_vertex_count - 1)
            for x in range(self.horizontal_vertex_count)
        ]

        edges = horizontal_edges + vertical_edges

        super(Plane, self).__init__(vertices, edges)
