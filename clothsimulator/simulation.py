import itertools

import numpy as np
from typing import List, Tuple

import clothsimulator.geometry as geo

G = 9.81


class MassVertex(geo.Vertex):
    """3D space mass point with physical properties.

    :param vertex_id: should be unique in one mesh
    """
    def __init__(self, vertex_id: int, x: float, y: float, z: float):
        super(MassVertex, self).__init__(vertex_id, x, y, z)
        self.velocity: np.ndarray = np.array([0.0, 0.0, 0.0])
        self.acceleration: np.ndarray = np.array([0.0, 0.0, 0.0])
        self.mass: float = 0  # in kilograms
        self.fixed: bool = False
        self.structural_springs: List[Spring] = []
        self.shear_springs: List[Spring] = []
        self.flexion_springs: List[Spring] = []

    def add_structural_spring(self, structural_spring):
        # type: (StructuralSpring) -> None
        self.structural_springs.append(structural_spring)

    def add_shear_spring(self, shear_spring):
        # type: (ShearSpring) -> None
        if shear_spring not in self.shear_springs:
            self.shear_springs.append(shear_spring)

    def add_flexion_spring(self, flexion_spring):
        # type: (FlexionSpring) -> None
        if flexion_spring not in self.flexion_springs:
            self.flexion_springs.append(flexion_spring)

    def animate(self, delta_t: float):
        """Evaluate new vertex position

        :param delta_t: seconds
        """
        if self.fixed:
            return
        self.velocity += self.acceleration * delta_t
        self.position += self.velocity * delta_t
        self.acceleration = np.array([0.0, 0.0, 0.0])

    def simulate(self):
        if self.fixed:
            return

        self.acceleration += self.get_gravitation_force_vector() / self.mass

        springs = self.structural_springs + self.shear_springs + self.flexion_springs
        for spring in springs:
            self.acceleration += spring.get_force_vector_for(self) / self.mass

    def get_gravitation_force_vector(self):
        return np.array([0.0, -(self.mass * G), 0.0])

    def update(self, position):
        x, y, z = position
        self.position = np.array([x, y, z])


class Cloth(geo.Mesh):
    """Cloth is mesh with physically based cloth simulation."""
    VERTEX_CLS = MassVertex
    DEFAULT_VERTEX_MASS = 0.0001
    vertex_mass = None
    vertices: List[MassVertex] = []

    def __init__(
            self,
            name: str,
            vertices: List[Tuple[float, float, float]],
            edges: List[Tuple[int, int]]
    ):
        super(Cloth, self).__init__(vertices, edges)
        self.name = name
        self.init_cloth(Cloth.vertex_mass)

    def init_cloth(self, vertex_mass):
        for v in self.vertices:
            v.mass = vertex_mass
        self.init_springs()

    def simulate(self, delta_t: float):
        """
        :param delta_t: seconds
        """
        for v in self.vertices:
            v.simulate()
        for v in self.vertices:
            v.animate(delta_t)

    def init_springs(self):
        self.init_structural_springs()
        self.init_shear_springs()
        self.init_flexion_springs()

    def init_structural_springs(self):
        for edge in self.edges:
            structural_spring = StructuralSpring(
                self.vertices[edge[0]], self.vertices[edge[1]]
            )
            self.vertices[edge[0]].add_structural_spring(structural_spring)
            self.vertices[edge[1]].add_structural_spring(structural_spring)

    def init_shear_springs(self):
        def iter_items_except_one(l, item):
            for x in l:
                if x == item:
                    continue
                yield x
        for v in self.vertices:
            for s1, s2 in itertools.combinations(v.structural_springs, 2):
                w1: MassVertex = s1.get_the_other(v)
                w2: MassVertex = s2.get_the_other(v)

                for ss1 in iter_items_except_one(w1.structural_springs, s1):
                    for ss2 in iter_items_except_one(w2.structural_springs, s2):
                        ww1 = ss1.get_the_other(w1)
                        ww2 = ss2.get_the_other(w2)
                        if ww1 == ww2:
                            spring = ShearSpring(v, ww1)
                            v.add_shear_spring(spring)
                            ww1.add_shear_spring(spring)

    def init_flexion_springs(self):
        for m in self.vertices:
            if len(m.structural_springs) < 4:
                continue
            for s in m.structural_springs:
                for s2 in m.structural_springs:
                    if s == s2:
                        continue
                    v = s.get_the_other(m)
                    v2 = s2.get_the_other(m)
                    e1 = v - m
                    e2 = v2 - m
                    angle = geo.angle(e1, e2)
                    if angle > FlexionSpring.ANGLE_THRESHOLD:
                        flexion_spring = FlexionSpring(v, v2, m)
                        v.add_flexion_spring(flexion_spring)
                        v2.add_flexion_spring(flexion_spring)

    def update(self, vertex_positions):
        for i, pos in enumerate(vertex_positions):
            self.vertices[i].update(vertex_positions[i])


class SpringError(Exception):
    pass


class Spring(object):
    """Abstract class."""
    stiffness: float = None
    damping_factor: float = None

    def __init__(self, vertex1: MassVertex, vertex2: MassVertex) -> None:
        self.vertex1: MassVertex = vertex1
        self.vertex2: MassVertex = vertex2

    def __eq__(self, other):
        assert self.__class__ == other.__class__
        return self.to_tuple() == other.to_tuple()

    def __str__(self):
        v1_id, v2_id = self.to_tuple()
        return "%s(%d, %d)" % (self.__class__.__name__, v1_id, v2_id)

    def __hash__(self):
        return hash(self.to_tuple())

    def to_tuple(self) -> Tuple[int, int]:
        return (
            (self.vertex1.id, self.vertex2.id)
            if self.vertex1.id <= self.vertex2.id
            else (self.vertex2.id, self.vertex1.id)
        )

    def get_the_other(self, vertex):
        return self.vertex2 if vertex == self.vertex1 else self.vertex1

    def check_vertex(self, vertex):
        if vertex not in [self.vertex1, self.vertex2]:
            raise SpringError("The vertex is not part of the spring.")

    def get_direction_from(self, vertex) -> np.ndarray:
        """Normalized vector pointing from the given Vertex to the other spring member Vertex."""
        self.check_vertex(vertex)
        from_vertex = vertex
        to_vertex = self.get_the_other(vertex)
        return geo.normalize(to_vertex.position - from_vertex.position)

    def get_force_vector_for(self, vertex) -> np.ndarray:
        raise NotImplementedError("Abstract method.")


class StretchSpring(Spring):
    """Abstract class."""
    def __init__(self, vertex1: MassVertex, vertex2: MassVertex) -> None:
        super(StretchSpring, self).__init__(vertex1, vertex2)
        self.rest_length: float = self.get_length()
        self.last_length: float = self.get_length()

    def get_length(self) -> float:
        return self.vertex1.distance_from(self.vertex2)

    def get_force_vector_for(self, vertex) -> np.ndarray:
        self.check_vertex(vertex)
        dir_to_other_vertex: np.ndarray = self.get_direction_from(vertex)
        current_length = self.get_length()
        spring_force = (current_length - self.rest_length) * self.stiffness
        if spring_force < 0.0:
            spring_force = 0.0
        spring_force_vector: np.ndarray = spring_force * dir_to_other_vertex

        lengthening_speed = current_length - self.last_length
        damping_force = lengthening_speed * self.damping_factor
        damping_force_vector = damping_force * dir_to_other_vertex
        # damping_force_vector always points to the other direction then
        # the particle is moving relatively to the other spring member

        self.last_length = current_length

        return spring_force_vector + damping_force_vector


class StructuralSpring(StretchSpring):
    DEFAULT_STIFFNESS: float = 0.2
    DEFAULT_DAMPING_FACTOR: float = 0.02


class ShearSpring(StretchSpring):
    DEFAULT_STIFFNESS: float = 0.2
    DEFAULT_DAMPING_FACTOR: float = 0.02


class FlexionSpring(Spring):
    DEFAULT_STIFFNESS: float = 0.02
    DEFAULT_DAMPING_FACTOR: float = 0.0

    ANGLE_THRESHOLD = np.deg2rad(170)

    def __init__(
            self,
            vertex1: MassVertex,
            vertex2: MassVertex,
            vertex_middle: MassVertex,
    ) -> None:
        super(FlexionSpring, self).__init__(vertex1, vertex2)
        self.vertex_middle = vertex_middle

    def get_force_vector_for(self, vertex) -> np.ndarray:
        e1 = vertex - self.vertex_middle
        e2 = self.get_the_other(vertex) - self.vertex_middle
        angle = geo.angle(e1, e2)
        max_angle = np.deg2rad(180)
        force = self.stiffness * ((max_angle - angle) / max_angle)

        perpendicular = np.cross(e1, e2)
        force_dir = np.cross(e1, perpendicular)

        return force_dir * force


