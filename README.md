# Table of Content
1. [About this project](#example)
2. [Directory Structure](#example2)
3. [Setup Project](#third-example)
    1. [Create python virtual environment](#create-python-virtual-environment)
    2. [Setup JetBrains PyCharm](#setup-jetbrains-pycharm)
    3. [Install and run](#install-and-run)
    4. [Unit tests](#unit-tests)
4. [Application usage](#application-usage)
    1. [Parameters](#parameters) 
    1. [Scenes](#scenes) 
    1. [Shortcuts](#shortcuts) 
    1. [Examples](#examples) 

# About this project
This project was done for MMA (Multimedia and Computer Animation) class
at FEL CVUT (FEE CTU) at winter semester 2018/2019.

http://vyuka.iim.cz/a4m39mma:a4m39mma


My name is Jordan Moravenov. If you need more information, contact me: j.moravenov(at)gmail.com.

**This project implements mass-spring model for cloth simulation.**

Bellow you will find directory structure of this repository, 
information about running this program and interacting with it as well as detailed information 
on how to setup developer environment to dig in deeper in the code of this project.

# Directory Structure

```
clothsimulator             ... root directory of the project
  |- .idea                 ... JetBrains PyCharm project files
  |- clothsimulator        ... source files
  |    `- test             ... unit tests using nosetest
  |- doc                   ... references, documentation, project report etc.
  |    `- references       ... pdf articles and links to usefull educational sources
  `- recorded_simulations  ... simulations baked using clothsimulator
```

# Setup Project

## Create python virtual environment
```bash
sudo pip install virtualenv
sudo pip install virtualenvwrapper
sudo mkdir /opt/py-venv
sudo chmod 777 /opt/py-venv

# put these two lines .bashrc (if bash is the terminal you use)
export WORKON_HOME=/opt/py-venv
source /usr/local/bin/virtualenvwrapper.sh

# make virtual environment with python 3
sudo apt install python3-distutils  # this may not be needed
mkvirtualenv --python=`where python3` mma

# here is how to exit the virtual environment:
deactivate

# and how to enter the virtual environment again:
workon mma
```
## Setup JetBrains PyCharm
(optional)

setup the virtual environment in Jetbrains PyCharm: File -> Settings (ctrl+alt+s) -> Project: clothsimulator -> Project Interpreter -> Project Interpreters -> Show All -> + (add new) -> existing environment -> ```/opt/py-venv/mma/bin/python```

## Install and run

```bash
# build
python setup.py sdist
# install 
pip install dist/clothsimulator-1.0.0.tar.gz --upgrade
# run
clothsimulator
```

**Run ```clothsimulator --help``` for more information about possible program parameters.**

```clothsimulator``` command will run the main() function of the program.

```clothsimulator_demo``` command will run the demo_main() function, which exists for testing purposes.

For development purposes you can run the program from directly from source code without installation:
```bash
python -m clothsimulator.main
```

Alternatively you can run ```main.py``` script directly from PyCharm (Shift + F10) with no need of rebuild or install.

More about how to use this application in section [Application usage](#application-usage)

## Unit tests
Nosetest is used for unit testing.

Here is how to run the tests.

```bash
nosetests  # basic
nosetests -v  # more reasonable output
nosetests -v --rednose  # color output if rednose is installed
```

To install rednose (for color output) run:

```bash
pip install rednose
```
Now you can use the ```--rednose``` switch with ```nosetests```.

# Application usage
## Parameters

```clothsimulator -h``` will output following list of possible command line arguments.

```
usage: clothsimulator [-h] [--record DIRECTORY | --play FILE]
                      [--record-filename FILE_NAME] [--recording-time SECONDS]
                      [--scene MODE] [--fps FRAMES_PER_SECOND]
                      [--simulation-fps SIMULATION_STEPS_PER_SECOND]
                      [--playback-speed PLAYBACK_SPEED]
                      [--window-width PIXEL_COUNT] [--hide-axis]
                      [--structural-stiffness STRUCTURAL_STIFFNESS]
                      [--structural-damping STRUCTURAL_DAMPING]
                      [--shear-stiffness SHEAR_STIFFNESS]
                      [--shear-damping SHEAR_DAMPING]
                      [--flexion-stiffness FLEXION_STIFFNESS]
                      [--vertex-mass KILOGRAMS]

Cloth simulation program (mass-spring model)

optional arguments:
  -h, --help            show this help message and exit
  --record DIRECTORY, -r DIRECTORY
                        Directory, where the recorded animation will be saved.
                        The application enters recording mode when this
                        argument is used.
  --play FILE, -p FILE  File containing recorded animation, that will be
                        played. The application enters playing mode when this
                        argument is used.
  --record-filename FILE_NAME, -f FILE_NAME
                        Name of the saved animation file. (default:
                        simulation.record)
  --recording-time SECONDS, -t SECONDS
                        Number of seconds of the simulation to be recorded.If
                        not specified, the simulation will wait for you to
                        save it manually by pressing S.
  --scene MODE          Choose from a predefined scenes. This option is
                        ignored in playing mode. (default: flag_pinned_up)
                        OPTIONS: 'flag_pinned_up', 'flag_pinned_down',
                        'string_pinned_one', 'string_pinned_two', 'weight'
  --fps FRAMES_PER_SECOND
                        How often the image is rendered to the screen. In
                        recording mode it also specifies the FPS for the
                        recorded animation. In playing mode it also affects
                        the playback speed. Rather use the same FPS for
                        recording and playing and change playback speed using
                        --playback-speed (default: 25)
  --simulation-fps SIMULATION_STEPS_PER_SECOND, -s SIMULATION_STEPS_PER_SECOND
                        How many simulation steps will be calculated per
                        second. (default: 250)
  --playback-speed PLAYBACK_SPEED, -v PLAYBACK_SPEED
                        Set playback speed. 1.0 will play it in recorded
                        speed. 0.5 will play the animation twice slower, 2.0
                        will play the animation twice faster.
  --window-width PIXEL_COUNT, -w PIXEL_COUNT
                        Window width in pixels. (The window is rectangular)
  --hide-axis           Don't render 3D space axis.
  --structural-stiffness STRUCTURAL_STIFFNESS
                        default: 0.2
  --structural-damping STRUCTURAL_DAMPING
                        default: 0.02
  --shear-stiffness SHEAR_STIFFNESS
                        default: 0.2
  --shear-damping SHEAR_DAMPING
                        default: 0.02
  --flexion-stiffness FLEXION_STIFFNESS
                        default: 0.02
  --vertex-mass KILOGRAMS
                        default: 0.0001kg

``` 

By using different parameters it is possible to not only watch realtime simulation
but also record the simulation to a file and than play it.

There are several recorded simulations in ```clothsimulator/recorded_simulations```.

Please refer to ```clothsimulator/recorded_simulations/README.md``` for more information
about saved recorded simulations.

## Scenes
You can choose from several predefined scenes using ```--scene``` argument.

Allowed options: 'flag_pinned_up', 'flag_pinned_down', 'string_pinned_one', 'string_pinned_two', 'weight'
* **flag_pinned_up** ... cloth plane pinned by two upper vertices
* **flag_pinned_down** ... cloth plane pinned by two bottom vertices
* **string_pinned_one** ... one long string pinned by one vertex
* **string_pinned_two** ... one long string pinned by two vertices
* **weight** ... two vertices connected with a structural string; the bottom vertex has
    large mass (500 * vertex_mass which you can change with ```--vertex-mass``` argument)

## Shortcuts
```
LMB := Left Mouse Button
MMB := Middle Mouse Button

MMB or CTRL + LMB ... rotate scene
S ................... save scene (works only in recording mode)
R ................... reset simulation (reset animation in playing mode)
ESC ................. quit application
```

## Examples
```bash
# play simulation saved in file long_simulation.record 4x faster
clothsimulator --play long_simulation.record --playback-speed 4
```
