PROBLÉMY
--------
- velký časový krok => nestabilita
  - možná bude třeba pořešit výkon... :-(
- akumulující se velký vektor při použití dampingu
  - sledovat jeden hmotný bod, kde se vezme
    (možná, když se moc vrcholy přiblíží)
  - nejspíš potřeba vytvořit umělé omezující podmínky, 
    které zabrání nekontrolovatelnému růstu

PLÁN
----
1) projít pixar tutoriál PŘESNĚ tak jak ho podávají
2) hledat si tutoriály o takových simulacích, abych pochopil jak se to má dělat
3) číst si články a vyzkoumat v čem je můj problém
   - nestabilita simulace, velký časový krok
   - články ze scholaru stahovat přes: https://dialog.cvut.cz/index.html
4) zkusit opravit
5) napsat o tom všem zprávu
