More references
---------------

Full-text of scholar articles download from CVUT network or from home through https://dialog.cvut.cz/index.html

Hair simulation lecture:
https://youtu.be/IYDbj5DTEWc

Cloth simulation scripts (stanford):
https://graphics.stanford.edu/~mdfisher/cloth.html

Cloth mass-spring simulation visualization:
https://www.youtube.com/watch?v=ib1vmRDs8Vw

PyOpenGL + pygame tutorial video series:
https://www.youtube.com/watch?v=R4n4NyDG2hI&list=PLQVvvaa0QuDdfGpqjkEJSeWKGCP31__wD&index=1

"Pixar in a box" mass-spring model tutorial series (with damping):
https://www.khanacademy.org/partner-content/pixar/simulation/hair-simulation-code/v/particles
